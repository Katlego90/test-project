void main() {
  var app1 = new App();
  app1.name = "Fnb";
  app1.category = "Banking";
  app1.developer = "FirstRand Limited";
  app1.year = 2012;

  app1.printAppDetails();
  print("\n");

  var app2 = new App();
  app2.name = "Snapscan";
  app2.category = "Banking";
  app2.developer = "FireId Payments";
  app2.year = 2013;

  app2.printAppDetails();
  print("\n");

  var app3 = new App();
  app3.name = "Live Inspect";
  app3.category = "Insurance";
  app3.developer = "Live Inspect";
  app3.year = 2014;

  app3.printAppDetails();
  print("\n");

  var app4 = new App();
  app4.name = "Wumdrop";
  app4.category = "Logistics/Delivery";
  app4.developer = "Wumdrop";
  app4.year = 2015;

  app4.printAppDetails();
  print("\n");

  var app5 = new App();
  app5.name = "Domestly";
  app5.category = "Lifestyle";
  app5.developer = "Domestly";
  app5.year = 2016;

  app5.printAppDetails();
  print("\n");

  var app6 = new App();
  app6.name = "Shyft";
  app6.category = "Finance";
  app6.developer = "Standard Bank";
  app6.year = 2017;

  app6.printAppDetails();
  print("\n");

  var app7 = new App();
  app7.name = "Khula ecosystems";
  app7.category = "Agriculture";
  app7.developer = "Khula";
  app7.year = 2018;

  app7.printAppDetails();
  print("\n");

  var app8 = new App();
  app8.name = "Naked Insurance";
  app8.category = "Insurance";
  app8.developer = "Naked Financial Technology";
  app8.year = 2019;

  app8.printAppDetails();
  print("\n");

  var app9 = new App();
  app9.name = "EasyEquities";
  app9.category = "Insurance";
  app9.developer = "First World Trader";
  app9.year = 2020;

  app9.printAppDetails();
  print("\n");

  var app10 = new App();
  app10.name = "Ambani";
  app10.category = "Edutech";
  app10.developer = "Ambani Africa";
  app10.year = 2021;

  app10.printAppDetails();
  print("\n");

  app1.capitalizeAppName();
  app2.capitalizeAppName();
  app3.capitalizeAppName();
  app4.capitalizeAppName();
  app5.capitalizeAppName();
  app6.capitalizeAppName();
  app7.capitalizeAppName();
  app8.capitalizeAppName();
  app9.capitalizeAppName();
  app10.capitalizeAppName();
}

class App {
  String name = "";
  String category = "";
  String developer = "";
  int year = 0;

  void printAppDetails() {
    print("The name of the app is $name");
    print("The category of the app is $category");
    print("The app developer is $developer");
    print("App won MTN Business App of the Year Awards in $year");
  }

  void capitalizeAppName() {
    print(name.toUpperCase());
  }
}
